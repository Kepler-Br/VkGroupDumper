from sqliteWrapper import SqlWrapper
import logging
import yaml
import os
import vk
from time import sleep

class VKGroupFetcher(object):
	def __init__(self, token):
		self.logger = logging.getLogger("VKGroupFetcher")
		self.session = vk.Session(access_token=token)
		self.api = vk.API(self.session, v='5.35')
		self.__serializeSettings()
			

	def __getTotalMembers(self, groupId):
		response = self.api.groups.getMembers(group_id=groupId, sort="id_asc", 
	offset=0, count="1")
		return response["count"]

	def __serializeSettings(self):
		self.__filters = {"sex": None, "city": None, "country": None}
		with open("./settings.yaml", 'r') as file:
			self.__settings = yaml.load(file)
		if "filters" not in self.__settings:
			return
		 

		sexes = {"female": 1, "male": 2, "middle": 0}
		if "sex" in self.__settings["filters"]:
			if self.__settings["filters"]["sex"] != None:
				if self.__settings["filters"]["sex"].lower() in sexes:
					desiredSex = self.__settings["filters"]["sex"].lower()
					self.__filters["sex"] = sexes[desiredSex]

		if "city" in self.__settings["filters"]:
			if self.__settings["filters"]["city"] != None:
				desiredCity = self.__settings["filters"]["city"]
				desiredCity = desiredCity.capitalize()
				self.__filters["city"] = desiredCity


		if "country" in self.__settings["filters"]:
			if self.__settings["filters"]["country"] != None:
				desiredCountry = self.__settings["filters"]["country"]
				desiredCountry = desiredCountry.capitalize()
				self.__filters["country"] = desiredCountry


	def fetchGroupMembers(self, group, step=1000):
		totalMembers = self.__getTotalMembers(group["id"])
		output = []
		for i in range(0, totalMembers, step):
			while True:
				try:
					response = self.api.groups.getMembers(group_id=group["id"], sort="id_asc", 
														  offset=i, count=step, fields="relation,bdate,sex,city,country")
					break
				except socket.timeout as error:
					self.logger.error("\nRequest timed out. Waiting for a 2 sec.")
					sleep(2.0)
				except requests.exceptions.ReadTimeout as error:
					self.logger.error("\nRead timed out. Waiting for a 2 sec.")
					sleep(2.0)
				except requests.exceptions.ConnectTimeout as error:
					self.logger.error("\nRequest timed out. Waiting for a 2 sec.")
					sleep(2.0)
				except requests.exceptions.ConnectionError as error:
					self.logger.error("\nConnection error. Waiting for a 2 sec.")
					sleep(2.0)

			filtered = self.__filter(response["items"])
			output.extend(filtered)
			if i % 100 == 0:
				print("\r{0:.2f}%".format(i/totalMembers*100), end='')
			sleep(0.2)

		print("\r100%")
		return output

	def __filter(self, chunk):
		output = []
		for user in chunk:
			if "deactivated" in user:
				continue

			if self.__filters["sex"] != None:
				if self.__filters["sex"] != user["sex"]:
					continue

			if self.__filters["city"] != None:
				if "city" not in user:
					continue
				if self.__filters["city"] != user["city"]["title"]:
					continue

			if self.__filters["country"] != None:
				if "country" not in user:
					continue
				if self.__filters["country"] != user["country"]["title"]:
					continue

			output.append(user)
		return output