import sqlite3
from sqlite3 import Error
import logging

class SqlWrapper(object):
	def __init__(self):
		self.logger = logging.getLogger("SqlWrapper")
		try:
			self.conn = sqlite3.connect("vkGroups.db")
		except Error as e:
			self.logger.critical(e)
			exit(0)

	def createTable(self, groupName):
		sql_create_table = """CREATE TABLE IF NOT EXISTS {} (
									page_id integer PRIMARY KEY NULL,
									sex integer NOT NULL,
									first_name text NOT NULL,
									last_name text NOT NULL,
									city text NOT NULL,
									country text NOT NULL,
									birth_date text NOT NULL,
									relationship text NOT NULL
								);""".format(groupName)
		try:
			c = self.conn.cursor()
			c.execute(sql_create_table)
		except Error as e:
			self.logger.critical(e)
			exit(0)

	def addUser(self, groupName, user):
		sql_addUser = """INSERT INTO {}
		 				VALUES(?,?,?,?,?,?,?,?)""".format(groupName)
		cur = self.conn.cursor()
		page_id = int(user["id"])
		sex = "Null"
		if "sex" in user:
			genders = {0: "Null", 1: "Female", 2: "Male"}
			sex = genders[user["sex"]]

		first_name = user["first_name"]
		last_name = user["last_name"]
		city = "Null"
		if "city" in user:
			city = user["city"]["title"]
		country = "Null"
		if "country" in user:
			country = user["country"]["title"]
		birth_date = "Null"
		if "bdate" in user:
			birth_date = user["bdate"]
		relationship = "Null"
		if "relation" in user:
			relationStates = {0: "Null", 1: "Single", 2: "In relationship", 
							3: "Engaged", 4: "Married", 
							5: "It's complicated", 6: "Actively searching", 
							7: "In love"}
			relationship = relationStates[user["relation"]]
		try:
			cur.execute(sql_addUser, (page_id, sex, first_name, last_name, city, country, birth_date, relationship))
		except sqlite3.IntegrityError as e:
			self.logger.warning("{}".format(str(e)))
			self.logger.warning("With user: {}".format(user))
		cur.close()

	def commit(self):
		self.conn.commit()

	def checkTableExists(self, tablename):
		cur = self.conn.cursor()
		cur.execute("""
			SELECT count(*) FROM sqlite_master WHERE type = 'table' AND name = '{}'
			""".format(tablename))
		if cur.fetchone()[0] == 1:
			cur.close()
			return True
		cur.close()
		return False

	def dropTable(self, tablename):
		command = "DROP TABLE IF EXISTS {}".format(tablename)
		cur = self.conn.cursor()
		cur.execute(command)
		cur.close()
