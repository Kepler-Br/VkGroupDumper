from sqliteWrapper import SqlWrapper
from VKGroupFetcher import VKGroupFetcher
import logging
import yaml
import os

class VKGroupDumper(object):
	"""A class that coordinates VKGroupFetcher and sqliteWrapper.
	This class downloads members from groups listed in groupList.yaml"""
	def __init__(self):
		self.logger = logging.getLogger("VKGroupDumper")
		self.__serializeSettings()
		self.sqlWrapper = SqlWrapper()
		self.groupFetcher = VKGroupFetcher(self.settings["token"])
		

	def __serializeSettings(self):
		"""Load settings and grouplist."""
		with open("./settings.yaml", 'r') as file:
			self.settings = yaml.load(file)
		with open("./groupList.yaml", "r") as file:
			self.groupList = yaml.load(file)

	def __commitMembersToDB(self, members, group):
		"""Commit all fetched users to sqlite3 data base."""
		for user in members:
			self.sqlWrapper.addUser(group["name"], user)
		self.sqlWrapper.commit()

	def run(self):
		for group in self.groupList:
			self.logger.info("Fetching group members.")
			self.logger.info("Total groups in list: {}.".format(len(self.groupList)))
			for group in self.groupList:
				self.logger.info("Fetching group {} (https://vk.com/public{})".format(group["name"], group["id"]))
				if self.sqlWrapper.checkTableExists(group["name"]):
					print("The {} table is already exists.".format(group["name"]))
					inp = ""
					while True:
						inp = input("Refetch?(y/N)").lower()

						if inp in ["n", "y", ""]:
							break
					if inp in ["n", ""]:
						continue
					if inp == "y":
						self.sqlWrapper.dropTable(group["name"])
						print("Refetching...")

				self.sqlWrapper.createTable(group["name"])
				members = self.groupFetcher.fetchGroupMembers(group)
				self.logger.info("Done fetching.")
				self.logger.info("Commiting users to DB".format(group["name"]))
				self.__commitMembersToDB(members, group)
				self.logger.info("Done.")
		print("Dumping done. Have a good day.")
				
				

def createConfig():
	"""Create config files if not existing."""
	logger = logging.getLogger(__name__)
	if not os.path.isfile("groupList.yaml"):
		with open("./groupList.yaml", 'w') as file:
			file.write("#- {id: 1, name: 'example1'}\n#- {id: 2, name: 'example2'}")
		logger.warning("No groupList.yaml file. Creating example and exiting.")
		exit(1)

	if not os.path.isfile("settings.yaml"):
		with open("./settings.yaml", 'w') as file:
			file.write("""{\n"token": '0',\n"log_level": "info",\n"filters":{"sex": null, "city": null, "country": null}\n}""")
		logger.warning("No settings.yaml file. Creating example and exiting.")
		exit(1)

def setLoglevelFromFile():
	logger = logging.getLogger(__name__)
	with open("./settings.yaml", 'r') as file:
		settings = yaml.load(file)
	if "log_level" in settings:
		logLevels = {"criitical": 50, "error": 40, "warning": 30, "info": 20, "debug": 10}
		if settings["log_level"].lower() not in logLevels:
			logger.warning("No log_level specified in settings.yaml. Setting log_level to INFO.")
			logging.basicConfig(level=logging.INFO)
			logger.info("Setting log_level to INFO.")
		logging.basicConfig(level=logLevels[settings["log_level"]])
		logger.debug("Set loglevel to {}".format(settings["log_level"]))
	else:
		logger.warning("No log_level specified in settings.yaml. Setting log_level to INFO.")
		logging.basicConfig(level=logging.INFO)
		logger.info("Setting log_level to INFO.")

def main():
	createConfig()
	setLoglevelFromFile()
	groupDumber = VKGroupDumper()
	groupDumber.run()

	return 0

if __name__ == "__main__":
	exitCode = main()
	exit(exitCode)